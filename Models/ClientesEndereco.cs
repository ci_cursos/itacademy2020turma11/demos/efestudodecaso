﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class ClientesEndereco
    {
        public ClientesEndereco()
        {
            Pedidos = new HashSet<Pedido>();
        }

        public decimal CodCliente { get; set; }
        public decimal CodEndereco { get; set; }
        public DateTime DataCadastro { get; set; }

        public virtual Cliente CodClienteNavigation { get; set; }
        public virtual Endereco CodEnderecoNavigation { get; set; }
        public virtual ICollection<Pedido> Pedidos { get; set; }
    }
}
