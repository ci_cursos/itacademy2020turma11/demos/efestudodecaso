﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Produto
    {
        public Produto()
        {
            AutoresProdutos = new HashSet<AutoresProduto>();
            PedidosProdutos = new HashSet<PedidosProduto>();
        }

        public decimal CodProduto { get; set; }
        public string Titulo { get; set; }
        public DateTime AnoLancamento { get; set; }
        public string Importado { get; set; }
        public decimal Preco { get; set; }
        public decimal PrazoEntrega { get; set; }

        public virtual ICollection<AutoresProduto> AutoresProdutos { get; set; }
        public virtual ICollection<PedidosProduto> PedidosProdutos { get; set; }
    }
}
