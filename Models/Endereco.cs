﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Endereco
    {
        public Endereco()
        {
            ClientesEnderecos = new HashSet<ClientesEndereco>();
        }

        public decimal CodEndereco { get; set; }
        public string Rua { get; set; }
        public decimal Numero { get; set; }
        public string Complemento { get; set; }
        public decimal CodCidade { get; set; }
        public string Cep { get; set; }

        public virtual Cidade CodCidadeNavigation { get; set; }
        public virtual ICollection<ClientesEndereco> ClientesEnderecos { get; set; }
    }
}
