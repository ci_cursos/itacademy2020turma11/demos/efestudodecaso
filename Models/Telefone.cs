﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Telefone
    {
        public decimal CodCliente { get; set; }
        public decimal CodTelefone { get; set; }
        public decimal CodTipoTelefone { get; set; }
        public decimal? Ddd { get; set; }
        public string Numero { get; set; }

        public virtual Cliente CodClienteNavigation { get; set; }
        public virtual TiposTelefone CodTipoTelefoneNavigation { get; set; }
    }
}
