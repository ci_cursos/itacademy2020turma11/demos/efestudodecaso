﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Estado
    {
        public Estado()
        {
            Cidades = new HashSet<Cidade>();
        }

        public string Uf { get; set; }
        public string Nome { get; set; }
        public string Regiao { get; set; }

        public virtual ICollection<Cidade> Cidades { get; set; }
    }
}
