﻿using System;
using System.Collections.Generic;

#nullable disable

namespace efestudodecaso.Models
{
    public partial class Administrador
    {
        public decimal CodAdministrador { get; set; }
        public decimal NivelPrivilegio { get; set; }

        public virtual Usuario CodAdministradorNavigation { get; set; }
    }
}
